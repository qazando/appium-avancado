package com.test.screen;

import com.test.Hooks;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.remote.RemoteWebElement;

import java.util.HashMap;

public class ScreenLogin extends BaseScreen {

    @AndroidFindBy(id = "login_username")
    @iOSFindBy(accessibility = "login_username")
    private RemoteWebElement fieldEmail;

    @AndroidFindBy(id = "login_password")
    @iOSFindBy(accessibility = "login_password")
    private RemoteWebElement fieldPass;

    @AndroidFindBy(id = "login_button")
    @iOSFindBy(accessibility = "login_button")
    public RemoteWebElement btnLogin;

    @AndroidFindBy(id = "login_button")
    @iOSFindBy(xpath = "(//XCUIElementTypeOther[@name=\"55555\"])[2]")
    public RemoteWebElement elementoios;

    public void writeEmail() {
        fieldEmail.sendKeys("qazando@gmail.com");
    }

    public void writePass(String txt) {
        fieldPass.sendKeys(txt);
    }

    public void writeEmail2(String txt) {
        fieldEmail.sendKeys(txt);
    }

}