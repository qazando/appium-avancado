#language: pt

  @login
Funcionalidade: Login de Usuario Melhoria de cenário bonito

  Contexto: Logar no app
    Dado que eu visualize o texto "CONTINUAR" no botão login
    Dado que eu escreva as informações do usuário

  @login-sucesso
  Cenario: Login com sucesso
    Quando escrever os dados da senha com "123456"

  @test-falha
  Esquema do Cenário: Login com sucesso
    Dado que eu preencha o email com o usuario "<usuario>"
    Quando escrever os dados da senha com "<senha>"

    Exemplos:
      | senha    |  usuario                |
      | 123456   |  qazando@gmail.com      |
      | 23423423 |  qazando123@gmail.com   |
      | 1231234  |  herbertao@gmail.com    |


