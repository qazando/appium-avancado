$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("login2.feature");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "#language: pt"
    }
  ],
  "line": 4,
  "name": "Login de Usuario Melhoria de cenário bonito 2",
  "description": "",
  "id": "login-de-usuario-melhoria-de-cenário-bonito-2",
  "keyword": "Funcionalidade",
  "tags": [
    {
      "line": 3,
      "name": "@login-2"
    }
  ]
});
formatter.before({
  "duration": 25307745200,
  "status": "passed"
});
formatter.background({
  "line": 6,
  "name": "Logar no app",
  "description": "",
  "type": "background",
  "keyword": "Contexto"
});
formatter.step({
  "line": 7,
  "name": "que eu escreva as informações do usuário",
  "keyword": "Dado "
});
formatter.match({
  "location": "LoginSteps.que_eu_escreva_as_informações_do_usuário()"
});
formatter.result({
  "duration": 2780034900,
  "status": "passed"
});
formatter.scenario({
  "line": 10,
  "name": "Login com sucesso 2",
  "description": "",
  "id": "login-de-usuario-melhoria-de-cenário-bonito-2;login-com-sucesso-2",
  "type": "scenario",
  "keyword": "Cenario",
  "tags": [
    {
      "line": 9,
      "name": "@login-sucesso-2"
    }
  ]
});
formatter.step({
  "line": 11,
  "name": "escrever os dados da senha com \"123456\"",
  "keyword": "Quando "
});
formatter.match({
  "arguments": [
    {
      "val": "123456",
      "offset": 32
    }
  ],
  "location": "LoginSteps.escrever_os_dados_da_senha_com(String)"
});
formatter.result({
  "duration": 2028260300,
  "status": "passed"
});
});